const express = require('express')
const api = express.Router()
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'products not found'


api.get('/', (req, res) => {
  var db = req.db
  var collection = db.get('usercollection');

  collection.find({},{},function(e,docs){
    res.render('user/userHome/index.ejs', {
        "productsMongo" : docs
    });
  });
})

api.get('/payment', (req, res) => {
  res.render('user/userHome/payment.ejs')
})

module.exports = api
